import os
import sys
import json
import torch

from utils.localization import *
from utils.load_data import load_ds
from magnified_CD import POCausalRecon

device = "cuda" if torch.cuda.is_available() else "cpu"
with open('./configs/general_configs.json', 'r') as f:
    cfgs = json.load(f)
dataset_config = cfgs['ds_parameters']
model_config = {"model_hyperparams": {}, "training_hyperparams": {}}
model_config["model_hyperparams"] = cfgs['model_hyperparams']
model_config["training_hyperparams"] = cfgs['training_hyperparams']
dataset = load_ds("CRACs", "./datasets", 0, dataset_config, model_config, False) #, RC.download_dataset)
model = POCausalRecon(
    "PORCA",
    dataset.variables,
    36,
    "v01_savedir",
    device,
    **model_config["model_hyperparams"]  
)
model.run_train_with_HAS(dataset, model_config["training_hyperparams"])
directed_adj, bidirected_adj = model.get_admg_matrices(most_likely_graph=True, samples=1)
print(directed_adj)

if model.MLP_weights:
    directed_adj, bidirected_adj = model.get_admg_matrices(most_likely_graph=True, samples=1)
    loss_list = model.get_run()
    adj_matrix = directed_adj
    front_end = 3 # adjust according to the alarms from dataset
    t = 765 # adjust according to the alarms from dataset
    visit_counts = random_walk(front_end, adj_matrix, 0.2, 1000)
    anomaly_rank = calculate_rank(loss_list, t)
    topk_indices = weighted_rank_topk(visit_counts, anomaly_rank)
else:
    directed_adj, bidirected_adj = model.get_admg_matrices(most_likely_graph=True, samples=1)
    adj_matrix = directed_adj
    front_end = 3 # adjust according to the alarms from dataset
    visit_counts = random_walk(front_end, adj_matrix, 0.2, 1000)


import numpy as np
import torch
from torch.utils.tensorboard import SummaryWriter
import os
import time
from typing import List, Optional, Tuple, Dict, Any
from collections import defaultdict

os.chdir("../..")

from base.models.CD_leaner import CD_LEARNER
from scheduling_net import schedulingNet
from base.datasets.variables import Variables
from base.models.deci.variational_distributions import VarDistA_ENCO, VarDistA_ENCO_ADMG
from base.models.deci.base_distributions import GaussianBase, TemporalConditionalSplineFlow
from base.datasets.dataset import Dataset
from base.utils.training_objectives import get_input_and_scoring_masks
from base.models.deci.deci import _log_epoch_metrics


class POCausalRecon(CD_LEARNER):

    def __init__(
        self,
        model_id: str,
        variables: Variables,
        latent_variable_num: int,  
        *args,
        **kwargs,
    ):
        super().__init__(model_id, variables, latent_variable_num, *args, **kwargs)
        
        self.weights = None
        self.MLP_weights = 0

        if isinstance(self.var_dist_A, VarDistA_ENCO):
            self.var_dist_A: VarDistA_ENCO_ADMG = VarDistA_ENCO_ADMG(
                device = self.device,
                input_dim = self.observed_num_nodes,
                tau_gumbel = self.tau_gumbel,
                dense_init = self.dense_init
            )

    def get_magnified_matrices_tensor(
        self,
        do_round: bool = True,   
        samples: int = 100, 
        most_likely_graph: bool = False 
    ) -> tuple[torch.Tensor, torch.Tensor]:

        if most_likely_graph:
            directed_adj_samples = [self.var_dist_A.get_directed_adj_matrix(do_round=do_round)]
            bidirected_adj_samples = [self.var_dist_A.get_bidirected_adj_matrix(do_round=do_round)]
        else:
            directed_adj_samples = [self.var_dist_A.sample_directed_adj() for _ in range(samples)]
            bidirected_adj_samples = [self.var_dist_A.sample_bidirected_adj() for _ in range(samples)]

            if do_round:
                directed_adj_samples = [directed_adj.round() for directed_adj in directed_adj_samples]
                bidirected_adj_samples = [bidirected_adj.round() for bidirected_adj in bidirected_adj_samples]

        directed_adj = torch.stack(directed_adj_samples, dim=0)
        bidirected_adj = torch.stack(bidirected_adj_samples, dim=0)
        return directed_adj, bidirected_adj

    def get_magnified_matrices(
        self,
        do_round: bool = True,  
        samples: int = 100,  
        most_likely_graph: bool = False  
    ) -> tuple[np.ndarray, np.ndarray]:
        directed_adj, bidirected_adj = self.get_magnified_matrices_tensor(
            do_round = do_round,
            samples = samples,
            most_likely_graph = most_likely_graph
        )

        return (
            directed_adj.detach().cpu().numpy().astype(np.float64),
            bidirected_adj.detach().cpu().numpy().astype(np.float64)
        )

    def compute_loss_with_weights(
        self,
        weights, 
        step: int,
        x: torch.Tensor,
        mask_train_batch: torch.Tensor,
        input_mask: torch.Tensor,
        num_samples: int,
        tracker: Dict,
        train_config_dict: Dict[str, Any],
        alpha: float = None,
        rho: float = None,
        adj_true: Optional[np.ndarray] = None,
        compute_cd_fscore: bool = False,
        **kwargs,
    ) -> Tuple[torch.Tensor, Dict]:
    
        beta = 1.0

        x_fill = x
        imputation_entropy = avg_reconstruction_err = torch.tensor(0.0, device=self.device)


        elbo_terms = self._ELBO_terms(x_fill)
        log_p_x_base = torch.mul(elbo_terms["log_p_x_base"], weights).mean(dim=0)
        log_p_u_base = torch.mul(elbo_terms["log_p_u_base"], weights).mean(dim=0)
        log_p_A = elbo_terms["log_p_A"] / num_samples
        log_q_A = elbo_terms["log_q_A"] / num_samples
        log_qu_x = torch.mul(elbo_terms["log_qu_x"],weights).mean(dim=0)

        log_p_u_weighed = log_p_u_base * beta
        log_qu_x_weighed = log_qu_x * beta

        penalty_dag = elbo_terms["penalty_dag"] * alpha / num_samples
        penalty_dag += elbo_terms["penalty_dag"] * elbo_terms["penalty_dag"] * rho / (2 * num_samples)

        if train_config_dict["anneal_entropy"] == "linear":
            elbo = (
                log_p_x_base
                + log_p_u_weighed
                + imputation_entropy
                + log_p_A
                - log_q_A / max(5 - step, 1)
                - log_qu_x_weighed / max(5 - step, 1)
                - penalty_dag
            )
        elif train_config_dict["anneal_entropy"] == "noanneal":
            elbo = (
                log_p_x_base + log_p_u_weighed + imputation_entropy + log_p_A - log_q_A - log_qu_x_weighed - penalty_dag
            )

        loss = -elbo + avg_reconstruction_err * train_config_dict["reconstruction_loss_factor"]

        tracker["loss"].append(loss.item())
        tracker["penalty_dag"].append(elbo_terms["penalty_dag"].item())
        tracker["penalty_dag_weighed"].append(penalty_dag.item())
        tracker["log_p_A_sparse"].append(log_p_A.item())
        tracker["log_p_x"].append(log_p_x_base.item())
        tracker["log_p_u"].append(log_p_u_base.item())
        tracker["log_p_u_weighed"].append(log_p_u_weighed.item())
        tracker["imputation_entropy"].append(imputation_entropy.item())
        tracker["log_q_A"].append(log_q_A.item())
        tracker["log_qu_x"].append(log_qu_x.item())
        tracker["log_qu_x_weighed"].append(log_qu_x_weighed.item())
        tracker["reconstruction_mse"].append(avg_reconstruction_err.item())
        return loss, tracker


    # TODO:
    def optimize_inner_auglag_withinSPL(
        self,
        weights, 
        rho: float,
        alpha: float,
        beta: float,
        step: int,
        num_samples: int,
        dataloader,
        train_config_dict: Optional[Dict[str, Any]] = None,
        adj_true: Optional[np.ndarray] = None,
        bidirected_adj_true: Optional[np.ndarray] = None,
        n_spline_sample: int = 32,
        spline_ewma_alpha: float = 0.05,
    ):

        def get_lr():
            for param_group in self.opt.param_groups:
                return param_group["lr"]

        def set_lr(factor):
            for param_group in self.opt.param_groups:
                param_group["lr"] = param_group["lr"] * factor

        def initialize_lr():
            base_lr = train_config_dict["learning_rate"]
            for param_group in self.opt.param_groups:
                name = param_group["name"]
                param_group["lr"] = train_config_dict.get(f"{name}_learning_rate", base_lr)

        lim_updates_down = 3
        num_updates_lr_down = 0
        auglag_inner_early_stopping_lag = train_config_dict.get("auglag_inner_early_stopping_lag", 1500)
        auglag_inner_reduce_lr_lag = train_config_dict.get("auglag_inner_reduce_lr_lag", 500)
        initialize_lr()
        print("LR:", get_lr())
        best_loss = np.nan
        last_updated = -1
        done_opt = False

        tracker_loss_terms: Dict = defaultdict(list)
        inner_step = 0


        while inner_step < train_config_dict["max_auglag_inner_epochs"]:  # and not done_steps:
            
            for idx, (x, mask_train_batch) in enumerate(dataloader):
            # for x, mask_train_batch in dataloader:
                input_mask, _ = get_input_and_scoring_masks(
                    mask_train_batch,
                    max_p_train_dropout=train_config_dict["max_p_train_dropout"],
                    score_imputation=True,
                    score_reconstruction=True,
                )

                loss, tracker_loss_terms = self.compute_loss_with_weights(
                    step,
                    weights[idx],
                    x,
                    mask_train_batch,
                    input_mask,
                    num_samples,
                    tracker_loss_terms,
                    train_config_dict,
                    alpha,
                    rho,
                    adj_true,
                    bidirected_adj_true=bidirected_adj_true,
                    beta=beta,
                    compute_cd_fscore=train_config_dict.get("compute_cd_fscore", False),
                )
                self.opt.zero_grad()
                loss.backward()
                self.opt.step()

                # For MSE metric, update an estimate of the spline means
                if not isinstance(self.likelihoods["continuous"], (GaussianBase, TemporalConditionalSplineFlow)):
                    error_dist_mean = self.likelihoods["continuous"].sample(n_spline_sample).mean(0)
                    self.spline_mean_ewma = (
                        spline_ewma_alpha * error_dist_mean + (1 - spline_ewma_alpha) * self.spline_mean_ewma
                    )

                inner_step += 1

                if int(inner_step) % 100 == 0:
                    self.print_tracker(inner_step, tracker_loss_terms)
                if int(inner_step) % 500 == 0:
                    break
                elif inner_step >= train_config_dict["max_auglag_inner_epochs"]:
                    break

            # Save if loss improved
            if np.isnan(best_loss) or np.mean(tracker_loss_terms["loss"][-10:]) < best_loss:
                best_loss = np.mean(tracker_loss_terms["loss"][-10:])
                best_inner_step = inner_step
            # Check if has to reduce step size
            if (
                inner_step >= best_inner_step + auglag_inner_reduce_lr_lag
                and inner_step >= last_updated + auglag_inner_reduce_lr_lag
            ):
                last_updated = inner_step
                num_updates_lr_down += 1
                set_lr(0.1)
                print(f"Reducing lr to {get_lr():.5f}")
                if num_updates_lr_down >= 2:
                    done_opt = True
                if num_updates_lr_down >= lim_updates_down:
                    done_opt = True
                    print(f"Exiting at inner step {inner_step}.")
                    # done_steps = True
                    break
            if inner_step >= best_inner_step + auglag_inner_early_stopping_lag:
                done_opt = True
                print(f"Exiting at inner step {inner_step}.")
                # done_steps = True
                break
            if np.any(np.isnan(tracker_loss_terms["loss"])):
                print(tracker_loss_terms)
                print("Loss is nan, I'm done.", flush=True)
                # done_steps = True
                break
        self.print_tracker(inner_step, tracker_loss_terms)
        print(f"Best model found at innner step {best_inner_step}, with Loss {best_loss:.2f}")
        return done_opt, tracker_loss_terms


    def get_idx_and_weights_by_loss(
        self,
        weights,  
        loss, 
    ):
        sp_mode = 'mixture'
        init_std_range = 4
        shrink_rate = 0.0001

        avg = torch.mean(loss)
        std = torch.std(loss)

        t = 30  
        if sp_mode != 'mixture':
            lambda_sp = avg + (init_std_range - t * shrink_rate ) * std
            if lambda_sp < avg + std:
                lambda_sp = avg + std

            easy_idx = torch.where(loss < lambda_sp)[0]
            hard_idx = torch.where(loss >= lambda_sp)[0]

            if sp_mode == 'hard':
                weights[easy_idx] = 1.0
                if len(hard_idx) > 0:
                    weights[hard_idx] = 0.0

            elif sp_mode == 'linear':
                weights[easy_idx] = 1.0 - loss[easy_idx] / lambda_sp
                if len(hard_idx) > 0:
                    weights[hard_idx] = 0.0

            elif sp_mode == 'huber':
                weights[easy_idx] = 1.0
                if len(hard_idx) > 0:
                    weights[hard_idx] = lambda_sp / loss[hard_idx]

        else:
            lambda1_sp = avg + 1.0 * std
            lambda2_sp = avg + (init_std_range - t * shrink_rate) * std
            if lambda2_sp < lambda1_sp:
                lambda2_sp = lambda1_sp
            t += 1
            easy_idx = torch.where(loss <= lambda1_sp)[0]
            median_idx = torch.where((loss > lambda1_sp) & (loss < lambda2_sp))[0]
            hard_idx = torch.where(loss >= lambda2_sp)[0]
            weights[easy_idx] = 1.0
            if len(hard_idx) > 0:
                weights[hard_idx] = 0.0
                # visualize_batch(batch=img_batch_tensor2numpy(raw[hard_idx].cpu().detach()[:, 6:9, :, :]))
            if len(median_idx) > 0:
                weights[median_idx] = (lambda1_sp * (lambda2_sp - loss[median_idx])) / (loss[median_idx] * (lambda2_sp - lambda1_sp))

        return weights


    def scheduling_step(self, train_loader,  model, train_config_dict = None ):
        
        global_epochs = train_config_dict['global_epochs'] if 'global_epochs' in train_config_dict else 5
        warmup_epoch = train_config_dict['warmup_epoch'] if 'warmup_epoch' in train_config_dict else 1
      
        lambda1  = train_config_dict['lambda1'] if 'lambda1' in train_config_dict else 1.0
        lrate  = train_config_dict['lrate'] if 'lrate' in train_config_dict else 1.0

        adp_model = schedulingNet()
        loop = range(global_epochs)

        for epoch in loop:
            reweight_list = []
            R_list=[]
            idx=[]
            for i, data in enumerate(train_loader):
                X = data[0]
                gumble_G = torch.rand(X.shape[0],1)
                # W_star = W_star.to(X.device)
                
                optimizer = torch.optim.Adam(adp_model.parameters(), lr=lrate)
                weights, biases, extra_params = model.get_parameters(mode="wbx")
                log_likelihood=model.compute_log_likelihood(X, weights, biases, extra_params)
                #likelihood=torch.exp(log_likelihood)
                R = -log_likelihood
                R = R.to(self.device)

                optimizer.zero_grad()
                reweight_list = adp_model(R**2)
                # loss l1 regularization
                R_list = R
                idx=data[1]
                loss = torch.mean(torch.mul(reweight_list,log_likelihood)) + lambda1*adp_model.adaptive_l2_reg()
                loss.backward()
                optimizer.step()
                loop.set_postfix(adaptive_loss=loss.item())
                
        weights = reweight_list
        return weights,idx

    def run_train_with_HAS(
        self,
        dataset: Dataset,
        train_config_dict = None, 
    ) -> None:

        best_log_p_x = -np.inf

        dataloader, num_samples = self._create_dataset_for_deci(
            dataset=dataset,
            train_config_dict=train_config_dict
        )
        train_output_dir = os.path.join(self.save_dir, "train_output")
        os.makedirs(train_output_dir, exist_ok=True)
        log_path = os.path.join(train_output_dir, "summary")
        writer = SummaryWriter(log_path, flush_secs=1)
        print("Saving logs to", log_path)

        rho = train_config_dict["rho"]
        alpha = train_config_dict["alpha"]
        progress_rate = train_config_dict["progress_rate"]
        base_beta = train_config_dict["beta"] if "beta" in train_config_dict else 1.0
        base_lr = train_config_dict["learning_rate"]
        anneal_beta = train_config_dict["anneal_beta"] if "anneal_beta" in train_config_dict else None
        anneal_beta_max_steps = (
            train_config_dict["anneal_beta_max_steps"]
            if "anneal_beta_max_steps" in train_config_dict
            else int(train_config_dict["max_steps_auglag"] / 2)
        )

        global_epochs = train_config_dict['global_epochs'] if 'global_epochs' in train_config_dict else 5
        warmup_epoch = train_config_dict['warmup_epoch'] if 'warmup_epoch' in train_config_dict else 1
        
        parameter_list = [
            {
                "params": module.parameters(),
                "lr": train_config_dict.get(f"{name}_learning_rate", base_lr),
                "name": name,
            }
            for name, module in self.named_children()
        ]

        self.opt = torch.optim.Adam(parameter_list)

        self.train()  

        for epoch in range(global_epochs):
            
            weights = torch.ones(dataset._train_data.shape[0]).cuda().type(torch.cuda.FloatTensor)

            if epoch >= warmup_epoch:
                self.eval()

                weighted_tracker_loss_terms: Dict = defaultdict(list)

                for idx, (x, mask_train_batch) in enumerate(dataloader):
                # for x, mask_train_batch in dataloader:
                    input_mask, _ = get_input_and_scoring_masks(
                        mask_train_batch,
                        max_p_train_dropout=train_config_dict["max_p_train_dropout"],
                        score_imputation=True,
                        score_reconstruction=True,
                    )

                    loss, weighted_tracker_loss_terms = self.compute_loss_with_weights(
                        step,
                        weights[idx],
                        x,
                        mask_train_batch,
                        input_mask,
                        num_samples,
                        weighted_tracker_loss_terms,
                        train_config_dict,
                        alpha,
                        rho,
                        adj_true,
                        bidirected_adj_true=bidirected_adj_true,
                        beta=beta,
                        compute_cd_fscore=train_config_dict.get("compute_cd_fscore", False),
                    )

                loss_for_reweighting = torch.tensor(weighted_tracker_loss_terms["reconstruction_mse"] )
                
                weights = self.get_idx_and_weights_by_loss(
                    weights=weights,
                    loss = loss_for_reweighting
                )

                self.weights = weights


                self.train()

            base_idx = 0
            dag_penalty_prev = float("inf")
            num_below_tol = 0
            num_max_rho = 0
            num_not_done = 0

 
            for step in range(train_config_dict["max_steps_auglag"]):

                # Stopping if DAG conditions satisfied
                patience_dag_reached = train_config_dict.get("patience_dag_reached", 5)
                patience_max_rho = train_config_dict.get("patience_max_rho", 3)
                if num_below_tol >= patience_dag_reached:
                    print(f"DAG penalty below tolerance for more than {patience_dag_reached} steps")
                    break
                elif num_max_rho >= patience_max_rho:
                    print(f"Above max rho for more than {patience_max_rho} steps")
                    break

                if rho >= train_config_dict["safety_rho"]:
                    num_max_rho += 1

                # Anneal beta.
                if anneal_beta == "linear":
                    beta = base_beta * min((step + 1) / anneal_beta_max_steps, 1.0)
                elif anneal_beta == "reverse":
                    beta = base_beta * max((anneal_beta_max_steps - step) / anneal_beta_max_steps, 0.2)
                else:
                    beta = base_beta

                adj_true = None
                bidirected_adj_true = None


                # Inner loop
                print(f"Auglag Step: {step}")

                print(f"Beta Value: {beta}")

                # Optimize adjacency for fixed rho and alpha
                outer_step_start_time = time.time()

                done_inner, tracker_loss_terms = self.optimize_inner_auglag_withinSPL(
                    weights,
                    rho, alpha, beta, step, num_samples, dataloader, train_config_dict, adj_true, bidirected_adj_true
                )
                
                
                outer_step_time = time.time() - outer_step_start_time
                dag_penalty = np.mean(tracker_loss_terms["penalty_dag"])

                print(f"Dag penalty after inner: {dag_penalty:.10f}")
                print("Time taken for this step", outer_step_time)

                directed_adjacency, bidirected_adjacency = self.get_magnified_matrices(
                    do_round=False,
                    most_likely_graph=True, 
                    samples=1
                )

                print("Unrounded directed matrix:")
                print(directed_adjacency)
                print("Unrounded bidirected matrix:")
                print(bidirected_adjacency)

                if done_inner or num_not_done == 1:
                    num_not_done = 0
                    if dag_penalty < train_config_dict["tol_dag"]:
                        num_below_tol += 1

                    with torch.no_grad():
                        if dag_penalty > dag_penalty_prev * progress_rate:
                            print(f"Updating rho, dag penalty prev: {dag_penalty_prev: .10f}")
                            rho *= 10.0
                        else:
                            print("Updating alpha.")
                            dag_penalty_prev = dag_penalty
                            alpha += rho * dag_penalty
                            if dag_penalty == 0.0:
                                alpha *= 5
                        if rho >= train_config_dict["safety_rho"]:
                            alpha *= 5
                        rho = min([rho, train_config_dict["safety_rho"]])
                        alpha = min([alpha, train_config_dict["safety_alpha"]])

                else:
                    num_not_done += 1
                    print("Not done inner optimization.")

                adj_metrics = {}

                avg_tracker_log_px = np.mean(tracker_loss_terms["log_p_x"])
                self.log_p_x.fill_(avg_tracker_log_px)
                if avg_tracker_log_px > best_log_p_x:
                    print(f"Saved new best checkpoint with {self.log_p_x} instead of {best_log_p_x}")
                    self.save(best=True)
                    best_log_p_x = avg_tracker_log_px

                base_idx = _log_epoch_metrics(writer, tracker_loss_terms, adj_metrics, step, outer_step_time, base_idx)

                self.save()

                if dag_penalty_prev is not None:
                    print(f"Dag penalty: {dag_penalty:.15f}")
                    print(f"Rho: {rho:.2f}, alpha: {alpha:.2f}")

        return


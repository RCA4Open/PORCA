import os
os.chdir("..")

from ..datasets.variables import Variable, Variables
from .deci.ddeci import DDECI

class CD_LEARNER(DDECI):
    def __init__(
        self,
        model_id: str,
        variables: Variables,
        latent_variable_num: int, 
        *args,
        **kwargs,      
    ):

        all_variables = to_add_latent_variables(variables, latent_variable_num)
        super().__init__(model_id, all_variables, *args, **kwargs)
        

def to_add_latent_variables(variables: Variables, num_latent_variables: int) -> Variables:

    latent_variables = [
        Variable(name=f"U{i}", query=True, type="continuous", lower=0.0, upper=0.0, is_latent=True)
        for i in range(num_latent_variables)
    ]

    return Variables(variables.as_list() + latent_variables)



import numpy as np
import torch


def random_walk(front_end, adjacency_matrix, p, num_steps):
    transition_matrix = np.transpose(adjacency_matrix) / np.sum(adjacency_matrix, axis=1, keepdims=True)
    visit_counts = np.zeros(adjacency_matrix.shape[0])
    current_node = front_end
    for _ in range(num_steps):
        visit_counts[current_node] += 1
        if np.random.rand() < p:
            current_node = front_end
        else:
            next_node = np.random.choice(adjacency_matrix.shape[0], p=transition_matrix[current_node])
            current_node = next_node

    return visit_counts


def get_rank(tensor, t):
    sorted_indices = torch.argsort(tensor)
    rank = torch.where(sorted_indices == t)[0].item() + 1
    return rank


def calculate_rank(tensor, t):
    d = tensor.shape[0]
    rank_list = []
    for i in range(d):
        cur_rank = get_rank(tensor[i,:], t)
        rank_list.append(cur_rank)
    return np.array(rank_list)


def weighted_rank_topk(node_visits, ranks, psi=0.5, topk=5):
    normalized_visits = node_visits / np.sum(node_visits)
    normalized_ranks = (ranks - np.min(ranks)) / (np.max(ranks) - np.min(ranks))
    weighted_sum = psi * normalized_visits + (1 - psi) * normalized_ranks
    sorted_indices = np.argsort(weighted_sum)[::-1]
    topk_indices = sorted_indices[:topk]
    return topk_indices
